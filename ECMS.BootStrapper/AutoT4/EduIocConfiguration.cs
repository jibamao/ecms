﻿


using ECMS.AppService.Implements;
using ECMS.AppService.Interfaces;
using Microsoft.Extensions.DependencyInjection;
namespace ECMS.BootStrapper
{

    public static partial class ECMSIocConfiguration
    {
        /// <summary>
        /// 注册DI服务
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddIocBase(IServiceCollection services)
		{
		     			  			 			  		   services.AddTransient<IMenuService, MenuService>();
		 			 			  		   services.AddTransient<IRoleGroupService, RoleGroupService>();
		 			 			  		   services.AddTransient<IRoleService, RoleService>();
		 			 			  		   services.AddTransient<IRoleWithRoleGroupService, RoleWithRoleGroupService>();
		 			 			  		   services.AddTransient<IRouteTemplateService, RouteTemplateService>();
		 			 			  		   services.AddTransient<IUserService, UserService>();
		 			 			  		   services.AddTransient<IUserWithRoleService, UserWithRoleService>();
		 			 
        return services;
		}
    }
		
}

