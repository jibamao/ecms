﻿using ECMS.Infrastructure.Caching;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace ECMS.BootStrapper
{
    /// <summary>
    /// IOC配置
    /// </summary>
    public partial class ECMSIocConfiguration
    {
        /// <summary>
        /// 注册DI服务
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddIocConfiguration(IServiceCollection services)
        {
            services.AddTransient<ICacheManager, MemoryCacheAdapter>();
            services = AddIocBase(services);


            return services;
        }
    }
}
