﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ECMS.Infrastructure.Messaging
{
    public class GuidRequest
    {
        /// <summary>
        /// GUID
        /// </summary>
        public Guid Guid { get; set; }
    }
}
