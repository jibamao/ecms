﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECMS.Infrastructure.Messaging
{
    public class GetPagingRequest
    {
        public GetPagingRequest(int pageIndex,int pageSize)
        {
            PageIndex = pageIndex;
            PageSize = pageSize;
        }
        /// <summary>
        /// 页码
        /// </summary>
        public int PageIndex { get; private set; }
        /// <summary>
        /// 每页的记录数
        /// </summary>
        public int PageSize { get; private set; }

    }
}
