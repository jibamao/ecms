﻿using ECMS.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECMS.Infrastructure.Messaging
{
    public class GetPagingResponse<TAggregateRoot> where TAggregateRoot : EntityCore
    {
        public Page<TAggregateRoot> Pages { get; set; }
    }
}
