﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ECMS.Infrastructure
{
    public class ExportRequestModel<T>
    {

        public List<ExportRowModel> RowRequest { get; set; }

        /// <summary>
        /// 导出人Id
        /// </summary>
        public string ExportUserId { get; set; }

        /// <summary>
        /// 导出模块名称
        /// </summary>
        public string ModuleName { get; set; }

        public T Where { get; set; }
    }

    public class ExportRowModel
    {
        public string RowName { get; set; }

        public string RowKey { get; set; }

        public string TableName { get; set; }
    }
}
