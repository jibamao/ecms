﻿using ECMS.Infrastructure.ClientData;
using ECMS.Infrastructure.Domain;
using System;

namespace ECMS.Infrastructure.Helper
{
    public class ObjectHelper
    {

        /// <summary>
        /// 将Page转成layui table能识别的格式
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="page"></param>
        /// <returns></returns>
        public static dynamic PageToLayuiTable<T>(ApiResult<Page<T>> page) where T : class
        {
            try
            {
                if (page == null)
                    throw new ArgumentNullException(nameof(page));

                return new
                {
                    code = page.Success ? 0 : 1,
                    msg = page.Message,
                    count = page.Success ? page.Data.TotalItems : 0,
                    data = page.Success ? page.Data.Items : null
                };
            }
            catch (Exception ex)
            {
                return new
                {
                    code = 1,
                    msg = ex.Message
                };
            }
        }
    }
}
