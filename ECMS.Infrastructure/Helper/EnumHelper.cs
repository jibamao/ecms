﻿using System;
using System.ComponentModel;
using System.Reflection;

namespace ECMS.Infrastructure.Helper
{
    /// <summary>
    /// 枚举帮助类
    /// </summary>
    public class EnumHelper
    {
        /// <summary>
        /// 获取枚举描述详情
        /// </summary>
        /// <param name="enumValue"></param>
        /// <returns></returns>
        public static string GetDescription(Enum enumValue)
        {
            try
            {
                string str = enumValue.ToString();
                FieldInfo field = enumValue.GetType().GetField(str);
                object[] objs = field.GetCustomAttributes(typeof(DescriptionAttribute), false);
                if (objs == null || objs.Length == 0)
                    return str;
                DescriptionAttribute da = (DescriptionAttribute)objs[0];
                return da.Description;
            }
            catch (Exception)
            {

                return "未设置";
            }

        }



    }


}
