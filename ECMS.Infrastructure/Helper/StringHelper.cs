﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ECMS.Infrastructure.Helper
{
    public class StringHelper
    {
        public static int GetAgeByIdCard(string str)
        {
            if (!VerificationHelper.IsIdCard(str))
                return 0;
            var year = DateTime.Now.Year;
            switch (str.Length)
            {
                case 15:
                    year = int.Parse("19" + str.Substring(6, 2));
                    break;
                case 18:
                    year = int.Parse(str.Substring(6, 4));
                    break;
            }
            return DateTime.Now.Year - year;
        }
    }
}
