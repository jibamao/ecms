﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace ECMS.Infrastructure.Provider.Interfaces
{
    public interface IServiceCore<T> where T : class
    {
        /// <summary>
        /// 逻辑删除
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        void DeleteForge(T entity);
        /// <summary>
        /// 查询对象
        /// </summary>
        /// <param name="isFilter">是否过滤已执行伪删除的数据，默认为true:过滤</param>
        /// <returns></returns>
        IQueryable<T> Query(bool isFilter = true);

        /// <summary>
        /// 逻辑删除
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="express"></param>
        /// <returns></returns>
        void DeleteForge(Expression<Func<T, bool>> express);
        /// <summary>
        /// 物理删除
        /// </summary>
        /// <param name="entity"></param>
        void Delete(T entity);
        /// <summary>
        /// 物理删除
        /// </summary>
        /// <param name="express"></param>
        void Delete(Expression<Func<T, bool>> express);
    }
}
