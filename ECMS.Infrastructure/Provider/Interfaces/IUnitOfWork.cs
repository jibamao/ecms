﻿using ECMS.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ECMS.Infrastructure.Provider.Interfaces
{
    public interface IUnitOfWork
    {
        void RegisterNew<TEntity>(TEntity entity) where TEntity : EntityCore;

        void RegisterDirty<TEntity>(TEntity entity) where TEntity : EntityCore;

        void RegisterClean<TEntity>(TEntity entity) where TEntity : EntityCore;

        void RegisterDeleted<TEntity>(TEntity entity) where TEntity : EntityCore;

        Task<bool> CommitAsync();

        bool Commit();

        void Rollback();
    }
}
