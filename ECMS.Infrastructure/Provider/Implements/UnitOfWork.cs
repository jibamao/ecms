﻿using ECMS.Infrastructure.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECMS.Infrastructure.Provider.Implements
{
    public class UnitOfWork
    {
        private Guid TransGuid { get; set; } = Guid.NewGuid();
        public DbContext DbContext { private get; set; }

        public void RegisterNew<TEntity>(TEntity entity)
             where TEntity : EntityCore
        {
            ThrowExceptionIfEntityIsInvalid(entity);
            DbContext.Set<TEntity>().Add(entity);
        }
        public async Task RegisterNewAsync<TEntity>(TEntity entity)
        where TEntity : EntityCore
        {
            ThrowExceptionIfEntityIsInvalid(entity);
            await DbContext.Set<TEntity>().AddAsync(entity);
        }
        public void RegisterNewRange<TEntity>(IEnumerable<TEntity> entities) where TEntity : EntityCore
        {
            if (entities == null || entities.Count() == 0)
                throw new ArgumentNullException(nameof(entities));
            foreach (var entity in entities)
            {
                ThrowExceptionIfEntityIsInvalid(entity);
            }
            DbContext.Set<TEntity>().AddRange(entities);
        }

        public async Task RegisterNewRangeAsync<TEntity>(IEnumerable<TEntity> entities) where TEntity : EntityCore
        {
            if (entities == null || entities.Count() == 0)
                throw new ArgumentNullException(nameof(entities));
            foreach (var entity in entities)
            {
                ThrowExceptionIfEntityIsInvalid(entity);
            }
            await DbContext.Set<TEntity>().AddRangeAsync(entities);
        }
        public void RegisterDirty<TEntity>(TEntity entity)
            where TEntity : EntityCore
        {
            ThrowExceptionIfEntityIsInvalid(entity);
            var entry = DbContext.Entry(entity);
            entry.State = EntityState.Modified;
            entity.LastChangeTime = DateTime.Now;
        }

        public void RegisterClean<TEntity>(TEntity entity)
            where TEntity : EntityCore
        {
            var entry = DbContext.Entry(entity);
            entry.State = EntityState.Unchanged;
        }

        public void RegisterDeleted<TEntity>(TEntity entity)
            where TEntity : EntityCore
        {
            DbContext.Set<TEntity>().Remove(entity);
        }

        public async Task<bool> CommitAsync()
        {
            var isSuccess = await DbContext.SaveChangesAsync() > 0;
            return isSuccess;
        }

        public void Rollback()
        {
            throw new NotImplementedException();
        }

        public bool Commit()
        {
            var isSuccess = DbContext.SaveChanges() > 0;
            return isSuccess;
        }

        private string PropertyValuesToModel(PropertyValues values)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("{");
            foreach (var propertyName in values.Properties)
            {
                stringBuilder.Append(string.Format("\"{0}\": \"{1}\",", propertyName.Name, values[propertyName.Name]));
            }
            var rstring = stringBuilder.ToString().TrimEnd(',') + "}";
            return rstring;
        }

        /// <summary>
        /// 验证数据的正确性
        /// </summary>
        /// <param name="entity">实体</param>
        private void ThrowExceptionIfEntityIsInvalid<TEntity>(TEntity entity) where TEntity : EntityCore
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));
            if (entity.GetBrokenRules().Any())
            {
                var brokenRules = new StringBuilder();
                brokenRules.AppendLine("数据验证不通过，错误信息：");
                foreach (var businessRule in entity.GetBrokenRules())
                {
                    brokenRules.AppendLine(businessRule.Rule);
                }
                throw new Exception(brokenRules.ToString());
            }
        }
    }
}
