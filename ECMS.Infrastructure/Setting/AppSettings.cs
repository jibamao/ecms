﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ECMS.Infrastructure.Setting
{
    public class AppSettings
    {
        public string ItemOne { get; set; }
        public string ItemTwo { get; set; }

        public string ExcelAddress { get; set; }
    }
}
