﻿using ECMS.Infrastructure.Engines;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace ECMS.Infrastructure.Domain.Events
{
    public class DomainEventHandlerFactory
    {
        public static IEnumerable<IdomainEventHandler<T>> GetDomainEventHandlersFor<T>(T domainEvent)
            where T : IdomainEvent
        {
            return EngineContainerFactory.GetContainer().GetAllInstances<IdomainEventHandler<T>>();
        }
    }
}
