﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ECMS.Infrastructure.Domain.Events
{
    /// <summary>
    /// 领域事件处理接口
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IdomainEventHandler<in T> where T : IdomainEvent
    {
        /// <summary>
        /// 事件处理
        /// </summary>
        /// <param name="domainEvent"></param>
        void Handler(T domainEvent);
    }
}
