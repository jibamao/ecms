﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace ECMS.Infrastructure.Domain.Events
{
    public static class DomainEvents
    {
        public static void Raise<T>(T domainEvent) where T : IdomainEvent
        {
            DomainEventHandlerFactory
                .GetDomainEventHandlersFor(domainEvent)
                .ForEach(h => h.Handler(domainEvent));
        }
    }
}
