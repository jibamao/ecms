﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ECMS.Infrastructure.Domain.Events
{
    /// <summary>
    /// 领域事件发布接口
    /// </summary>
    public interface IdomainEventPublisher
    {
        /// <summary>
        /// 发布事件
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="domainEvent"></param>
        void Publish<T>(T domainEvent) where T : IdomainEvent;
    }
}
