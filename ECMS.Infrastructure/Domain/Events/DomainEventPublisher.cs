﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ECMS.Infrastructure.Domain.Events
{
    public class DomainEventPublisher : IdomainEventPublisher
    {
        public void Publish<T>(T domainEvent) where T : IdomainEvent
        {
            DomainEventHandlerFactory
                .GetDomainEventHandlersFor(domainEvent)
                .ForEach(h => h.Handler(domainEvent));
        }
    }
}
