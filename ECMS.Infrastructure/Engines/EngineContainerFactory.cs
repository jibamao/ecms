﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ECMS.Infrastructure.Engines
{
    /// <summary>
    /// Ioc容器引擎工厂类
    /// </summary>
    public class EngineContainerFactory
    {
        private static IEngine _container;

        /// <summary>
        /// 初始化
        /// </summary>
        /// <param name="container"></param>
        public static void InitializeEngineContainerFactory(IEngine container)
        {
            _container = container;
        }
        /// <summary>
        /// 获取Ioc容器的实例
        /// </summary>
        /// <returns></returns>
        public static IEngine GetContainer()
        {
            return _container;
        }
        /// <summary>
        /// 当前上下文
        /// </summary>
        public static IEngine Context
        {
            get
            {
                return _container;
            }
        }
    }
}
