﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ECMS.Infrastructure.Engines
{
    /// <summary>
    /// Ioc容器引擎接口
    /// </summary>
    public interface IEngine
    {
        /// <summary>
        /// 获取单个实例
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        object GetInstance(Type type);
        /// <summary>
        /// 获取单个实例
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        object TryGetInstance(Type type);
        /// <summary>
        /// 获取多个实例
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="type"></param>
        /// <returns></returns>
        IEnumerable<T> GetAllInstances<T>(Type type);
        void Release(object instance);
    }
    /// <summary>
    /// [扩展]Ioc容器
    /// </summary>
    public static class EngineExtensions
    {
        /// <summary>
        /// 获取实例
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="container"></param>
        /// <returns></returns>
        public static T GetInstance<T>(this IEngine container)
        {
            return (T)container.GetInstance(typeof(T));
        }
        /// <summary>
        /// 获取实例
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="container"></param>
        /// <returns></returns>
        public static IEnumerable<T> GetAllInstances<T>(this IEngine container)
        {
            return container.GetAllInstances<T>(typeof(T));
        }
    }
}
