﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Text;

namespace ECMS.Infrastructure.Engines
{
    public class AutofacEngineContainer : IEngine
    {
        public AutofacEngineContainer(IContainer container)
        {
            _container = container ?? throw new ArgumentNullException(nameof(container));
        }

        private readonly IContainer _container;
        public IEnumerable<T> GetAllInstances<T>(Type type)
        {
            throw new NotImplementedException();
        }

        public object GetInstance(Type type)
        {
            return _container.Resolve(type);
        }

        public void Release(object instance)
        {
            throw new NotImplementedException();
        }

        public object TryGetInstance(Type type)
        {
            object instance;
            _container.TryResolve(type, out instance);
            return instance;
        }
    }
}
