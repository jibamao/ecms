﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using StructureMap;

namespace ECMS.Infrastructure.Engines
{
    public class EngineContainer : IEngine
    {
        public EngineContainer(IContainer container)
        {
            _container = container ?? throw new ArgumentNullException(nameof(container));
        }

        private readonly IContainer _container;

        public object GetInstance(Type type)
        {
            if (type == null)
                return null;
            try
            {
                return _container.GetInstance(type);
            }
            catch (StructureMapException ex)
            {
                var message = ex.Message + "\n" + _container.WhatDoIHave();
                throw new Exception(message);
            }
        }

        public object TryGetInstance(Type type)
        {
            if (type == null)
                return null;
            try
            {
                return _container.TryGetInstance(type);
            }
            catch (StructureMapException ex)
            {
                var message = ex.Message + "\n" + _container.WhatDoIHave();
                throw new Exception(message);
            }
        }

        public IEnumerable<T> GetAllInstances<T>(Type type)
        {
            try
            {
                return _container.GetAllInstances(type).Cast<T>();
            }
            catch (StructureMapException ex)
            {
                var message = ex.Message + "\n" + _container.WhatDoIHave();
                throw new Exception(message);
            }
        }

        public void Release(object instance)
        {
            _container.Release(instance);
        }
    }
}
