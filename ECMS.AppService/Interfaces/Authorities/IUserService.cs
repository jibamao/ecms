﻿using ECMS.AppService.Model;
using ECMS.AppService.Model.Models;
using ECMS.AppService.Model.Responses;
using ECMS.AppService.Model.Requests;
using ECMS.Domain.Models;
using ECMS.Infrastructure.ClientData;
using ECMS.Infrastructure.Domain;
using ECMS.Infrastructure.Provider.Interfaces;
using System;
using System.Threading.Tasks;

namespace ECMS.AppService.Interfaces
{
    public partial interface IUserService
    {
        Task<ApiResult<SaveResponse>> SaveAsync(UserModel model);
        Task<ApiResult<Page<UserResponse>>> GetPagesAsync(GetUserPagesRequest request);
        Task<UserResponse> GetDetailAsync(Guid guid);
        Task<ApiResult<string>> DeleteAsync(Guid? guid);


        Task<ApiResult<UserResponse>> LoginAsync(LoginRequest request);
    }
}
