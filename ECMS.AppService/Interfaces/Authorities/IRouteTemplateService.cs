﻿using ECMS.AppService.Model.Models;
using ECMS.AppService.Model.Requests;
using ECMS.AppService.Model.Responses;
using ECMS.Infrastructure.ClientData;
using ECMS.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ECMS.AppService.Interfaces
{
    public partial interface IRouteTemplateService
    {
        Task<ApiResult<SaveResponse>> SaveAsync(RouteTemplateModel model);
        Task<ApiResult<Page<RouteTemplateResponse>>> GetPagesAsync(GetRouteTemplatePagesRequest request);
        Task<ApiResult<IList<RouteTemplateResponse>>> GetListsAsync();
        Task<ApiResult<RouteTemplateResponse>> GetDetailAsync(Guid guid);
        Task<ApiResult<string>> DeleteAsync(Guid guid);

        
    }
}
