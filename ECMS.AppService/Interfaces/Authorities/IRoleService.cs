﻿using ECMS.AppService.Model.Models;
using ECMS.AppService.Model.Requests;
using ECMS.AppService.Model.Responses;
using ECMS.Infrastructure.ClientData;
using ECMS.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ECMS.AppService.Interfaces
{
    public partial interface IRoleService
    {
        Task<ApiResult<SaveResponse>> SaveAsync(RoleModel model);
        Task<ApiResult<Page<RoleResponse>>> GetPagesAsync(GetRolePagesRequest request);
        Task<ApiResult<RoleResponse>> GetDetailAsync(Guid guid);
        Task<ApiResult<string>> DeleteAsync(Guid guid);
    }
}
