﻿using ECMS.AppService.Model.Models;
using ECMS.AppService.Model.Requests;
using ECMS.AppService.Model.Responses;
using ECMS.Infrastructure.ClientData;
using ECMS.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ECMS.AppService.Interfaces
{
    public partial interface IMenuService
    {
        Task<ApiResult<SaveResponse>> SaveAsync(MenuModel model);
        Task<ApiResult<Page<MenuResponse>>> GetPagesAsync(GetMenuPagesRequest request);
        Task<ApiResult<MenuResponse>> GetDetailAsync(Guid guid);
        Task<ApiResult<string>> DeleteAsync(Guid guid);
        /// <summary>
        /// 递归读取菜单列表  
        /// </summary>
        /// <returns></returns>
        Task<ApiResult<IList<MenuRecursionResponse>>> GetListsWithRecursion();
    }
}
