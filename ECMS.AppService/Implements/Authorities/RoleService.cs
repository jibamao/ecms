﻿using ECMS.AppService.Model.Models;
using ECMS.AppService.Model.Requests;
using ECMS.AppService.Model.Responses;
using ECMS.Domain.Models;
using ECMS.Infrastructure.ClientData;
using ECMS.Infrastructure.Domain;
using ECMS.Infrastructure.Extensions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ECMS.AppService.Implements
{
    public partial class RoleService
    {
        public async Task<ApiResult<SaveResponse>> SaveAsync(RoleModel model)
        {
            var response = new ApiResult<SaveResponse>();
            try
            {
                if (model == null)
                    throw new ArgumentNullException(nameof(model));
                Role entity;
                if (model.Guid == null)
                {
                    entity = new Role
                    {
                        Name = model.Name,
                        Remark = model.Remark
                    };
                    await _unitofwork.RegisterNewAsync(entity);
                }
                else
                {
                    entity = await Query().FirstOrDefaultAsync(p => p.Guid == model.Guid);
                    if (entity == null)
                    {
                        response.Message = "找不到要修改的数据.";
                        return response;
                    }
                    entity.Name = model.Name;
                    entity.Remark = model.Remark;

                    _unitofwork.RegisterDirty(entity);
                }

                var flag = await _unitofwork.CommitAsync();
                response.Success = flag;
                response.Message = flag ? "保存成功." : "保存失败.";
                response.Data = new SaveResponse
                {
                    Guid = entity.Guid,
                    Id = entity.Id
                };
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                response.StatusCode = 500;
            }
            return response;
        }
        public async Task<ApiResult<Page<RoleResponse>>> GetPagesAsync(GetRolePagesRequest request)
        {
            var response = new ApiResult<Page<RoleResponse>>();
            try
            {
                if (request == null)
                    throw new ArgumentNullException(nameof(request));

                // 读取数据
                var result = await Query()
                    .ToPageAsync(request.PageIndex, request.PageSize);

                if (result.Items == null || result.Items.Count == 0)
                {
                    response.Message = "暂无数据.";
                    return response;
                }
                response.Success = true;
                response.Message = "获取成功.";

                // 转换数据
                response.Data = result.ToViewPage(p => new RoleResponse
                {
                    Name = p.Name,
                    Remark = p.Remark,
                    CreateTime = p.CreateTime.ToString("yyyy-MM-dd HH:mm:ss"),
                    Guid = p.Guid,
                    Id = p.Id,
                });
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                response.StatusCode = 500;
            }
            return response;
        }
        public async Task<ApiResult<RoleResponse>> GetDetailAsync(Guid guid)
        {
            var response = new ApiResult<RoleResponse>();
            try
            {
                if (guid == null)
                    throw new ArgumentNullException(nameof(guid));

                // 读取数据
                var single = await Query().FirstOrDefaultAsync(p => p.Guid == guid);

                if (single == null)
                {
                    response.Message = "找不到记录.";
                    return response;
                }
                response.Success = true;
                response.Message = "获取成功.";

                // 转换数据
                response.Data = new RoleResponse
                {
                    Name = single.Name,
                    Remark = single.Remark,
                    CreateTime = single.CreateTime.ToString("yyyy-MM-dd HH:mm:ss"),
                    Guid = single.Guid,
                    Id = single.Id,
                };
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                response.StatusCode = 500;
            }
            return response;
        }
        public async Task<ApiResult<string>> DeleteAsync(Guid guid)
        {
            var response = new ApiResult<string>();
            try
            {
                if (guid == null)
                    throw new ArgumentNullException(nameof(guid));

                // 物理删除
                Delete(p => p.Guid == guid);

                var flag = await _unitofwork.CommitAsync();
                response.Success = flag;
                response.Message = flag ? "删除成功." : "删除失败.";
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                response.StatusCode = 500;
            }
            return response;
        }
    }
}
