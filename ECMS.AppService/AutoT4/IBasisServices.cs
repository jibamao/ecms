﻿

using ECMS.Domain.Models;
using ECMS.Infrastructure.Provider.Interfaces;

namespace ECMS.AppService.Interfaces
{
    			  			 			  		  public partial  interface IMenuService : IServiceCore<Menu>
         {
      
         }	
		 			 			  		  public partial  interface IRoleGroupService : IServiceCore<RoleGroup>
         {
      
         }	
		 			 			  		  public partial  interface IRoleService : IServiceCore<Role>
         {
      
         }	
		 			 			  		  public partial  interface IRoleWithRoleGroupService : IServiceCore<RoleWithRoleGroup>
         {
      
         }	
		 			 			  		  public partial  interface IRouteTemplateService : IServiceCore<RouteTemplate>
         {
      
         }	
		 			 			  		  public partial  interface IUserService : IServiceCore<User>
         {
      
         }	
		 			 			  		  public partial  interface IUserWithRoleService : IServiceCore<UserWithRole>
         {
      
         }	
		 			 }


