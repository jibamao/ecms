﻿

using ECMS.AppService.Interfaces;
using ECMS.Domain;
using ECMS.Domain.Models;
using ECMS.Infrastructure.Provider.Implements;

namespace ECMS.AppService.Implements
{

  			  			 			     public partial class MenuService: ServiceCore<Menu>, IMenuService
    {
	  public MenuService(ECMSDbContext context) : base(context)
        {

        }

    }		
		 			 			     public partial class RoleGroupService: ServiceCore<RoleGroup>, IRoleGroupService
    {
	  public RoleGroupService(ECMSDbContext context) : base(context)
        {

        }

    }		
		 			 			     public partial class RoleService: ServiceCore<Role>, IRoleService
    {
	  public RoleService(ECMSDbContext context) : base(context)
        {

        }

    }		
		 			 			     public partial class RoleWithRoleGroupService: ServiceCore<RoleWithRoleGroup>, IRoleWithRoleGroupService
    {
	  public RoleWithRoleGroupService(ECMSDbContext context) : base(context)
        {

        }

    }		
		 			 			     public partial class RouteTemplateService: ServiceCore<RouteTemplate>, IRouteTemplateService
    {
	  public RouteTemplateService(ECMSDbContext context) : base(context)
        {

        }

    }		
		 			 			     public partial class UserService: ServiceCore<User>, IUserService
    {
	  public UserService(ECMSDbContext context) : base(context)
        {

        }

    }		
		 			 			     public partial class UserWithRoleService: ServiceCore<UserWithRole>, IUserWithRoleService
    {
	  public UserWithRoleService(ECMSDbContext context) : base(context)
        {

        }

    }		
		 			 }


