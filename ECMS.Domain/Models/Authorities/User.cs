﻿using ECMS.Infrastructure.Domain;
using System;

namespace ECMS.Domain.Models
{
    /// <summary>
    /// 用户
    /// </summary>
    public class User : EntityCore
    {
        /// <summary>
        /// 登录名
        /// </summary>
        public string LoginName { get; set; }
        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// 真实姓名
        /// </summary>
        public string RealName { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        public string Mobile { get; set; }
        /// <summary>
        /// 邮箱
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// 座机号
        /// </summary>
        public string Tel { get; set; }
        /// <summary>
        /// 是否启用IP限制
        /// </summary>
        public bool IPLimitEnabled { get; set; }
        /// <summary>
        /// 允许登录的IP地址,多个以逗号隔开
        /// </summary>
        public string AllowIPAddresses { get; set; }
        /// <summary>
        /// 帐号是否已锁定
        /// </summary>
        public bool LockoutEnabled { get; set; }
        /// <summary>
        /// 锁定结束的时间
        /// </summary>
        public DateTime LockoutEnd { get; set; } = new DateTime(1900, 1, 1);
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }


        protected override void Validate()
        {
            if (string.IsNullOrEmpty(LoginName))
                AddBrokenRule(new BusinessRule(nameof(LoginName), "登录名不能为空."));
            if (string.IsNullOrEmpty(Password))
                AddBrokenRule(new BusinessRule(nameof(Password), "登录密码不能为空."));
        }
    }
}
