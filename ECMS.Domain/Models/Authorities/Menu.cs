﻿using ECMS.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace ECMS.Domain.Models
{
    /// <summary>
    /// 菜单
    /// </summary>
    public class Menu : EntityCore
    {
        /// <summary>
        /// 显示的标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 地址，(a标签的href属性值)
        /// </summary>
        public string Path { get; set; }
        /// <summary>
        /// 图标，当前只支持layui图标值
        /// </summary>
        public string Icon { get; set; }
        /// <summary>
        /// 父级ID(重要)，一级菜单pid为0
        /// </summary>
        public int Pid { get; set; }
        /// <summary>
        /// 是否展开(该值只对拥有子菜单的菜单有效)
        /// </summary>
        public bool Open { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        protected override void Validate()
        {
            // throw new NotImplementedException();
        }
    }
}
