﻿using ECMS.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace ECMS.Domain.Models
{
    /// <summary>
    /// 用户与角色关联
    /// </summary>
    public class UserWithRole : EntityCore
    {
        /// <summary>
        /// 用户ID
        /// </summary>
        public int UserId { get; set; }
        public virtual User User { get; set; }
        /// <summary>
        /// 角色ID
        /// </summary>
        public int RoleId { get; set; }

        public virtual Role Role { get; set; }

        protected override void Validate()
        {
            // throw new NotImplementedException();
        }
    }
}
