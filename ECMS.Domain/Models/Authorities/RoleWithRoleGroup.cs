﻿using ECMS.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace ECMS.Domain.Models
{
    /// <summary>
    /// 角色与角色组关联模型
    /// </summary>
    public class RoleWithRoleGroup : EntityCore
    {
        /// <summary>
        /// 角色ID
        /// </summary>
        public int RoleId { get; set; }
        public virtual Role Role { get; set; }
        /// <summary>
        /// 角色组ID
        /// </summary>
        public int RoleGroupId { get; set; }
        public virtual RoleGroup RoleGroup { get; set; }

        protected override void Validate()
        {
            // throw new NotImplementedException();
        }
    }
}
