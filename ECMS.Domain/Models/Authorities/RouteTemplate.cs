﻿using ECMS.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace ECMS.Domain.Models
{
    /// <summary>
    /// 路由模板
    /// </summary>
    public class RouteTemplate : EntityCore
    {
        /// <summary>
        /// 路径
        /// </summary>
        public string Path { get; set; }
        /// <summary>
        /// 模块地址
        /// </summary>
        public string Component { get; set; }
        /// <summary>
        /// 模板名称
        /// </summary>
        public string Name { get; set; }

        protected override void Validate()
        {
            // throw new NotImplementedException();
        }
    }
}
