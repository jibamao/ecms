﻿using ECMS.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace ECMS.Domain.Models
{
    /// <summary>
    /// 角色模型
    /// </summary>
    public class Role : EntityCore
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }


        protected override void Validate()
        {
            // throw new NotImplementedException();
        }
    }
}
