﻿using ECMS.Domain.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace ECMS.Domain
{
    public class ECMSDbContext : DbContext
    {
        public ECMSDbContext(DbContextOptions<ECMSDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region Authorities
            modelBuilder.Entity<User>().ToTable("Users").Property(p => p.RowVersion).IsRowVersion();
            modelBuilder.Entity<RouteTemplate>().ToTable("RouteTemplates").Property(p => p.RowVersion).IsRowVersion();
            modelBuilder.Entity<Menu>().ToTable("Menus").Property(p => p.RowVersion).IsRowVersion();
            modelBuilder.Entity<Role>().ToTable("Roles").Property(p => p.RowVersion).IsRowVersion();
            modelBuilder.Entity<RoleGroup>().ToTable("RoleGroups").Property(p => p.RowVersion).IsRowVersion();
            modelBuilder.Entity<RoleWithRoleGroup>().ToTable("RoleWithRoleGroups").Property(p => p.RowVersion).IsRowVersion();
            modelBuilder.Entity<UserWithRole>().ToTable("UserWithRoles").Property(p => p.RowVersion).IsRowVersion();

            #endregion

        }
    }
}
