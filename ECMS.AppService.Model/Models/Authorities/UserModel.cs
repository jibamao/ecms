﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ECMS.AppService.Model.Models
{
    public class UserModel : ModelBase
    {
        /// <summary>
        /// 登录名
        /// </summary>
        public string LoginName { get; set; }
        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// 真实姓名
        /// </summary>
        public string RealName { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        public string Mobile { get; set; }
        /// <summary>
        /// 座机号
        /// </summary>
        public string Tel { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 电子邮箱
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// 是否启用IP限制
        /// </summary>
        public bool IPLimitEnabled { get; set; }
        /// <summary>
        /// 允许登录的IP地址,多个以逗号隔开
        /// </summary>
        public string AllowIPAddresses { get; set; }
        /// <summary>
        /// 帐号是否已锁定
        /// </summary>
        public bool LockoutEnabled { get; set; }
        /// <summary>
        /// 锁定结束的时间
        /// </summary>
        public DateTime LockoutEnd { get; set; }
    }
}
