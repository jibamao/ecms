﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ECMS.AppService.Model.Models
{
    public class RouteTemplateModel : ModelBase
    {
        /// <summary>
        /// 路径
        /// </summary>
        public string Path { get; set; }
        /// <summary>
        /// 模块地址
        /// </summary>
        public string Component { get; set; }
        /// <summary>
        /// 模板名称
        /// </summary>
        public string Name { get; set; }
    }
}
