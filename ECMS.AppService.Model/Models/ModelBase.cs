﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ECMS.AppService.Model.Models
{
    /// <summary>
    /// 模型基类
    /// </summary>
    public class ModelBase
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// GUID
        /// </summary>
        public Guid? Guid { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public string CreateTime { get; set; }
    }
}
