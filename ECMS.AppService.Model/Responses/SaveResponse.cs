﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ECMS.AppService.Model.Responses
{
    /// <summary>
    /// 保存的响应类
    /// </summary>
    public class SaveResponse
    {
        /// <summary>
        /// ID
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// GUID
        /// </summary>
        public Guid Guid { get; set; }
    }
}
