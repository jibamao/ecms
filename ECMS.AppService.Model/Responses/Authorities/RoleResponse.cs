﻿using ECMS.AppService.Model.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ECMS.AppService.Model.Responses
{
    public class RoleResponse : RoleModel
    {
        /// <summary>
        /// 创建时间
        /// </summary>
        public string CreateTime { get; set; }
    }
}
