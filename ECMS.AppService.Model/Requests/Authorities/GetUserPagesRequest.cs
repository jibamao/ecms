﻿using ECMS.Infrastructure.Messaging;
using System;
using System.Collections.Generic;
using System.Text;

namespace ECMS.AppService.Model.Requests
{
    public class GetUserPagesRequest : GetPagingRequest
    {
        public GetUserPagesRequest(int pageIndex, int pageSize) : base(pageIndex, pageSize)
        {
        }
        /// <summary>
        /// 登录名
        /// </summary>
        public string LoginName { get; set; }
        /// <summary>
        /// 真实姓名
        /// </summary>
        public string RealName { get; set; }
        /// <summary>
        /// 手机号
        /// </summary>
        public string Mobile { get; set; }
    }
}
