﻿using ECMS.Infrastructure.Messaging;
using System;
using System.Collections.Generic;
using System.Text;

namespace ECMS.AppService.Model.Requests
{
    public class GetRolePagesRequest : GetPagingRequest
    {
        public GetRolePagesRequest(int pageIndex, int pageSize) : base(pageIndex, pageSize)
        {
        }
    }
}
