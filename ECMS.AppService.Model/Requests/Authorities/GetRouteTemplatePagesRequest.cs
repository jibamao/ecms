﻿using ECMS.Infrastructure.Messaging;
using System;
using System.Collections.Generic;
using System.Text;

namespace ECMS.AppService.Model.Requests
{
    public class GetRouteTemplatePagesRequest : GetPagingRequest
    {
        public GetRouteTemplatePagesRequest(int pageIndex, int pageSize) : base(pageIndex, pageSize)
        {
        }
    }
}
