﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ECMS.AppService.Model.Requests
{
    /// <summary>
    /// 登录请求类
    /// </summary>
    public class LoginRequest
    {
        /// <summary>
        /// 登录名
        /// </summary>
        public string LoginName { get; set; }
        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// 当前登录IP
        /// </summary>
        public string LoginIP { get; set; }
        /// <summary>
        /// 记住
        /// </summary>
        public bool RememberMe { get; set; }
    }
}
