﻿using ECMS.Infrastructure.Messaging;
using System;
using System.Collections.Generic;
using System.Text;

namespace ECMS.AppService.Model.Requests
{
    public class GetMenuPagesRequest : GetPagingRequest
    {
        public GetMenuPagesRequest(int pageIndex, int pageSize) : base(pageIndex, pageSize)
        {
        }
    }
}
