layui.define(['element', 'sidebar', 'mockjs', 'menu', 'route', 'utils', 'component', 'kit', 'apiconfig', 'tabs'], function (exports) {
    var element = layui.element,
        utils = layui.utils,
        $ = layui.jquery,
        _ = layui.lodash,
        route = layui.route,
        layer = layui.layer,
        menu = layui.menu,
        tabs = layui.tabs,
        component = layui.component,
        apiconfig = layui.apiconfig;


    var loadType = 'SPA';

    var kit = layui.kit;
    kit.set({
        type: loadType
    }).init();

    // 监听头部右侧 nav
    component.on('nav(header_right)', function (_that) {
        var target = _that.elem.attr('kit-target');
        if (target === 'setting') {
            // 绑定sidebar
            layui.sidebar.render({
                elem: _that.elem,
                //content:'', 
                title: '设置',
                shade: true,
                // shadeClose:false,
                // direction: 'left'
                dynamicRender: true,
                url: 'views/setting.html',
                // width: '50%', //可以设置百分比和px
            });
        }
    });

    var Admin = function () {
        this.config = {
            elem: '#app'
        };
        this.version = '1.0.0';
    };

    Admin.prototype.ready = function (callback) {
        _private.routeInit();
        _private.menuInit();
        _private.tabsInit();

        if (location.hash === '') {
            utils.setUrlState('主页', '#/');
        }

        // 注入mock
        layui.mockjs.inject(APIs);

        // 执行回调函数
        typeof callback === 'function' && callback();
    }
    Admin.prototype.render = function () {
        var that = this;
        return that;
    }

    var _private = {
        routeInit: function () {
            // route.set({
            //   beforeRender: function (route) {
            //     // 此配置可以限制页面访问
            //     if (!utils.oneOf(route.path, ['/user/table', '/user/table2', '/'])) {
            //       return {
            //         id: new Date().getTime(),
            //         name: 'unauthorized',
            //         path: '/error/unauthorized',
            //         component: 'views/error/unauthorized.html'
            //       };
            //     }
            //     return route;
            //   }
            // });
            axios.get('/Home/GetRouteTemplates')
                .then(function (response) {
                    if (response.status === 500) {
                        console.log(response);
                        throw new Error(response.statusText);
                    }
                    return response.data;
                })
                .then(function (res) {
                    if (res.success) {
                        console.log('router init successed.');
                        route.setRoutes({
                            routes: res.data
                        });
                    }
                })
                .catch(function (err) {
                    console.log(err);
                });

            // 配置路由
            //route.setRoutes({
            //    routes: [{
            //        path: '/',
            //        component: 'Home/About',
            //        name: '控制面板'
            //    }, {
            //        path: '/user/my',
            //        component: 'Home/Contact',
            //        name: '个人中心'
            //    }, {
            //        path: '/user/list',
            //        component: 'User/List',
            //        name: '用户列表'
            //    }, {
            //        path: '/user/create',
            //        component: 'User/CreateOrEdit',
            //        name: '新增用户'
            //    }, {
            //        path: '/user/edit',
            //        component: 'User/Edit',
            //        name: '编辑用户'
            //    }, {
            //        path: '/role/list',
            //        component: 'Role/List',
            //        name: '角色列表'
            //    }, {
            //        path: '/role/create',
            //        component: 'Role/CreateOrEdit',
            //        name: '新增角色'
            //    }, {
            //        path: '/role/edit',
            //        component: 'Role/Edit',
            //        name: '编辑角色'
            //    }, {
            //        path: '/menu/list',
            //        component: 'Menu/List',
            //        name: '菜单列表'
            //    }, {
            //        path: '/menu/create',
            //        component: 'Menu/CreateOrEdit',
            //        name: '新增菜单'
            //    }, {
            //        path: '/menu/edit',
            //        component: 'Menu/Edit',
            //        name: '编辑菜单'
            //    }, {
            //        path: '/routetemplate/list',
            //        component: 'RouteTemplate/List',
            //        name: '路由模板列表'
            //    }, {
            //        path: '/routetemplate/create',
            //        component: 'RouteTemplate/CreateOrEdit',
            //        name: '新增路由模板'
            //    }, {
            //        path: '/routetemplate/edit',
            //        component: 'RouteTemplate/Edit',
            //        name: '编辑路由模板'
            //    }]
            //});
            return this;
        },
        menuInit: function () {
            var that = this;

            const { user } = apiconfig;
            const { getMenus } = user;
            // 配置menu
            menu.set({
                dynamicRender: true,
                elem: '#menu-box',
                isJump: loadType === 'SPA',
                onClicked: function (obj) {
                    if (loadType === 'TABS') {
                        if (!obj.hasChild) {
                            const { data } = obj;
                            const { href, layid } = data;
                            var r = route.getRoute(href);
                            if (r) {
                                tabs.add({
                                    id: layid,
                                    title: r.name,
                                    path: href,
                                    component: r.component,
                                    rendered: true,
                                    icon: '&#xe62e;'
                                }, function () {
                                    route.render(href);
                                });
                            }
                        }
                    }
                },
                remote: {
                    url: apiconfig.user.getMenus,
                    method: 'post'
                },
                cached: false
            }).render();
            return this;
        },
        tabsInit: function () {
            tabs.render();
        }
    }

    var admin = new Admin();
    admin.ready(function () {
        console.log('Init successed.');
        if (loadType === 'TABS') {
            route.render('#/');
        } else {
            route.render();
        }
        admin.render();
    });

    //输出admin接口
    exports('index', {});
});