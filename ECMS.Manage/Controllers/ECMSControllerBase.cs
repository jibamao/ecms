﻿using ECMS.AppService.Model.Responses;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ECMS.Manage.Controllers
{
    [Authorize]
    public class ECMSControllerBase : Controller
    {


        protected async Task SignInAsync(UserResponse user)
        {
            var claims = new List<Claim>{
                    // new Claim(ClaimTypes.Name, user.Email),
                    new Claim("LoginName", user.LoginName),
                    new Claim("Guid",user.Guid.ToString()),
                    new Claim(ClaimTypes.Role, "Administrator"),
                };

            var claimsIdentity = new ClaimsIdentity(
                claims, CookieAuthenticationDefaults.AuthenticationScheme);

            var authProperties = new AuthenticationProperties
            {
                // AllowRefresh = true,
                //AllowRefresh = <bool>,
                // Refreshing the authentication session should be allowed.

                // ExpiresUtc = DateTimeOffset.UtcNow.AddMinutes(2),
                // The time at which the authentication ticket expires. A 
                // value set here overrides the ExpireTimeSpan option of 
                // CookieAuthenticationOptions set with AddCookie.

                IsPersistent = true
                //IsPersistent = <bool>,
                // Whether the authentication session is persisted across 
                // multiple requests. Required when setting the 
                // ExpireTimeSpan option of CookieAuthenticationOptions 
                // set with AddCookie. Also required when setting 
                // ExpiresUtc.

                //IssuedUtc = <DateTimeOffset>,
                // The time at which the authentication ticket was issued.

                //RedirectUri = <string>
                // The full path or absolute URI to be used as an http 
                // redirect response value.
            };

            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(claimsIdentity), authProperties);
        }
    }
}
