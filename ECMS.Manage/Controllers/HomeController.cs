﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ECMS.Manage.Models;
using Microsoft.AspNetCore.Authorization;
using ECMS.AppService.Interfaces;
using ECMS.Infrastructure.ClientData;
using ECMS.AppService.Model.Responses;

namespace ECMS.Manage.Controllers
{
    public class HomeController : ECMSControllerBase
    {
        private readonly IMenuService _menuService;
        private readonly IRouteTemplateService _routeTemplateService;
        public HomeController(IMenuService menuService, IRouteTemplateService routeTemplateService)
        {
            _menuService = menuService;
            _routeTemplateService = routeTemplateService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }
        [AllowAnonymous]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        /// <summary>
        /// 获取菜单
        /// </summary>
        /// <returns></returns>
        public async Task<IList<MenuRecursionResponse>> GetMenus()
        {
            var response = await _menuService.GetListsWithRecursion();

            return response.Data;
        }

        public async Task<ApiResult<IList<RouteTemplateResponse>>> GetRouteTemplates()
        {
            return await _routeTemplateService.GetListsAsync();
        }
    }
}
