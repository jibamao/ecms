﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ECMS.AppService.Interfaces;
using ECMS.AppService.Model;
using ECMS.AppService.Model.Models;
using ECMS.AppService.Model.Responses;
using ECMS.AppService.Model.Requests;
using ECMS.Infrastructure.ClientData;
using ECMS.Manage.Models;
using Microsoft.AspNetCore.Mvc;
using ECMS.Infrastructure.Domain;
using ECMS.Infrastructure.Helper;
using ECMS.Infrastructure.Messaging;

namespace ECMS.Manage.Controllers
{
    public class MenuController : ECMSControllerBase
    {
        private readonly IMenuService _service;
        public MenuController(IMenuService service)
        {
            _service = service;
        }

        public IActionResult CreateOrEdit()
        {
            return View(new MenuModel());
        }
        public async Task<IActionResult> Edit(Guid? guid)
        {
            if (guid == null)
            {
                return View("CreateOrEdit", new MenuModel());
            }
            var single = await _service.GetDetailAsync(guid.Value);

            return View("CreateOrEdit", single.Data);
        }
        public IActionResult List()
        {
            return View();
        }

        #region Hander
        // [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<ApiResult<SaveResponse>> Save([FromBody]MenuModel model)
        {
            return await _service.SaveAsync(model);
        }
        [HttpPost]
        public async Task<dynamic> GetPages([FromBody]GetMenuPagesRequest request)
        {
            var page = await _service.GetPagesAsync(request);

            return ObjectHelper.PageToLayuiTable(page);
        }
        [HttpPost]
        public async Task<ApiResult<string>> Delete([FromBody]GuidRequest request)
        {
            return await _service.DeleteAsync(request.Guid);
        }
        #endregion
    }
}